__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython']
__version__ = 1.0
__distributor__ = ['Marp Softwares, CA']
__license__ = ['PSF', 'MIT']
__website__ = 'http://www.marp.me/'

# { Milestones
# } Milestones


# { Imports
from kivy.uix.screenmanager import Screen,ScreenManager,ScreenManagerException
from kivy.uix.floatlayout import FloatLayout
from kivy.app import App
# } Imports


# { Souce

class EggedMainObj(App):

    def screenManagerObj(self): #TODO: screenManagerx function <<
        """
        Main class for handling Screens
        """
        scr = ScreenManager()
        return scr

    def OptionsViewObj(self):
        """
        Option View Screen
        """
        OptionMain = Screen()
        return OptionMain


    def IntroViewObj(self):
        """
        Intro View Screen
        """
        IntroMain = Screen()
        return IntroMain


    def GameViewObj(self):
        """
        Game View Screen
        """
        GameMain = Screen()
        return GameMain


    def build(self):
        """
        Main Builder from App Class, Init Like func
        """

        self.title = "Egged - Mobile Game | Marp Softwares, CA"

        ManagerObj = self.screenManagerObj()


        return ManagerObj

# } Source



if __name__ == '__main__':
    EggedMainObj().run() # Start PinoyChat Application